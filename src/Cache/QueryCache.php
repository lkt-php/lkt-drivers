<?php

namespace Lkt\Drivers\Cache;

use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;

class QueryCache implements CacheControllerInterface
{
    use CacheControllerTrait;

    /**
     * @return int
     */
    public static function getCacheCode()
    {
        return trim(count(static::$CACHE));
    }

    /**
     * @param string $query
     * @return false|int|string
     */
    public static function getCacheCodeForQuery(string $query)
    {
        $key = array_search($query, static::$CACHE, true);
        if ($key !== false) {
            $key = trim($key);
        }
        return $key;
    }

    public static function getCache()
    {
        return static::$CACHE;
    }
}