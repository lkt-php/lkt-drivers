<?php

namespace Lkt\Drivers;

class MySql extends AbstractConnection
{
    use ConnectionCacheTrait;

    const PORT = 3306;

    public function connect()
    {
        if ($this->connection !== null) {
            return;
        }
        // Perform the connection
        try {
            $this->connection = new \PDO (
                "mysql:host={$this->host};dbname={$this->database};charset=utf8",
                $this->user,
                $this->password
            );

        } catch (\Exception $e) {
            die ('Connection to database failed');
        }
    }

    public function disconnect()
    {
        $this->connection = null;
    }

    public function query($sql = '', $replacements = [], $rememberTotal = '')
    {
        $this->connect();
        $sql = ConnectionHelper::sanitizeQuery($sql);
        $sql = ConnectionHelper::prepareParams($sql, $replacements);
        $sql = \str_replace('_LANG', '_' . LanguageDriver::getLang(), $sql);
        if ($rememberTotal !== '') {
            $sql = \preg_replace('/SELECT/i', 'SELECT SQL_CALC_FOUND_ROWS', $sql, 1);
            $sql .= '; SET @rows_' . $rememberTotal . ' = FOUND_ROWS();';
        }


        // check if cached
        if (!$this->forceRefresh && $this->hasCacheEnabled() && $this->inCache($sql)) {
            return $this->loadCache($sql);
        }

        // fetch
        $result = $this->connection->query($sql, \PDO::FETCH_ASSOC);

        if ($result === true || $result === false) {
            return $result;
        }

        $r = [];
        foreach ($result as $row) {
            $r[] = $row;
        }

        $this->storeCache($sql, $r);
        return $r;
    }

    public function getTotalRows($rememberTotal)
    {
        $result = $this->query('SELECT @rows_:key as founded', ['key' => $rememberTotal]);
        $r = reset($result);
        return $r['founded'];
    }

    public function getLastInsertedId()
    {
        if ($this->connection === null) {
            return 0;
        }
        return (int)$this->connection->lastInsertId();
    }

    public function toPhinx()
    {
        return [
            'adapter' => 'mysql',
            'host' => $this->host,
            'name' => $this->database,
            'user' => $this->user,
            'pass' => $this->password,
            'port' => $this->port,
            'charset' => 'utf8',
        ];
    }

    public static function makeUpdateParams(array $params = []) :string
    {
        $r = [];
        foreach ($params as $field => $value) {
            $v = addslashes(stripslashes($value));
            if (strpos($value, 'COMPRESS(') === 0){
                $r[] = "`{$field}`={$value}";
            } else {
                $r[] = "`{$field}`='{$v}'";
            }
        }
        return trim(implode(',', $r));
    }

    public static function makeSelectParams(array $params = [], string $table = '') :string
    {
        $r = [];
        $prepend = trim($table);
        if ($prepend !== '') {
            $prepend .= '.';
        }
        foreach ($params as $key => $param) {
            if (is_numeric($key)) {
                $r[] = "`{$prepend}{$param}`";
            } else {
                $r[] = "`{$prepend}{$key}` AS `{$param}`";
            }
        }
        return trim(implode(',', $r));
    }

    public static function makeInsertStatement(string $table, string $madeUpdateParams) :string
    {
        return "INSERT INTO {$table} SET {$madeUpdateParams}";
    }

    public static function makeUpdateStatement(string $table, string $madeUpdateParams, string $where) :string
    {
        return "UPDATE {$table} SET {$madeUpdateParams} WHERE {$where}";
    }

    public static function makeDeleteStatement(string $table, string $where) :string
    {
        return "DELETE FROM {$table} WHERE {$where}";
    }
}