<?php

namespace Lkt\Drivers;


class Curl extends AbstractConnection
{
    const USER_AGENT = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

    protected $headers = [];

    public function __construct($connectionInfo = [])
    {
        $connectionInfo = $this->parseConnectionInfo($connectionInfo);
        parent::__construct($connectionInfo);
        $this->headers = $connectionInfo['Headers'];
    }

    public function query($url = '', $args = [], $method = 'GET')
    {
        $ch = curl_init();

        if (!$method) {
            $method = 'GET';
        }

        curl_setopt($ch, CURLOPT_USERAGENT, static::USER_AGENT);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($this->user && $this->password){
            curl_setopt($ch, CURLOPT_USERPWD,  "{$this->user}:{$this->password}");
        }

        $link = $this->host . $url;
        $fieldsString = self::prepareFields($args);

        if (count($args) > 0) {
            if ($method === 'GET') {
                $link .= '?' . $fieldsString;
            } elseif ($method === 'POST') {
                curl_setopt($ch, CURLOPT_POST, count(explode('&', urldecode($fieldsString))) > 0);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsString);
            } elseif ($method === 'DELETE') {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsString);
            }
        }

        if ($method === 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        curl_setopt($ch, CURLOPT_URL, $link);

        if (count($this->headers) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        }

        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $result;
    }

    private static function prepareFields($fields = [])
    {
        $fields = is_array($fields) ? $fields : [];
        $count = count($fields);

        $r = '';
        if ($count > 0) {
            $r = http_build_query($fields);
        }
        return $r;
    }

    public function buildUri($url = '', $args = [], $method = 'GET')
    {
        $link = $this->host . $url;
        $fieldsString = self::prepareFields($args);
        if (count($args) > 0 && $method === 'GET') {
            $link .= '?' . $fieldsString;
        }
        return $link;
    }
}
