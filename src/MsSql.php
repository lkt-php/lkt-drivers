<?php

namespace Lkt\Drivers;

class MsSql extends AbstractConnection
{
    public function __construct($connectionInfo = [])
    {
        parent::__construct($connectionInfo);

        $args = [
            'CharacterSet' => $this->characterSet,
            'Database' => $this->database,
            'UID' => $this->user,
            'PWD' => $this->password,
        ];

        if (is_array($connectionInfo['args'])) {
            $args = array_merge($args, $connectionInfo['args']);
        }

        $this->connection = sqlsrv_connect($this->host, $args);

        if (!$this->connection) {
            unset($args['Database']);
            $this->connection = sqlsrv_connect($this->host, $args);
        }
    }

    public function query($sql = '')
    {
        $sql = ConnectionHelper::sanitizeQuery($sql);

        $getNames = sqlsrv_query($this->connection, $sql);
        if ($getNames === false) {
            die(ver(['sql' => $sql, 'errors' => sqlsrv_errors()], true));
        }
        if (!$getNames) {
            return [];
        }
        $items = [];

        while ($row = sqlsrv_fetch_array($getNames, SQLSRV_FETCH_ASSOC)) {
            $items[] = (object)$row;
        }
        sqlsrv_free_stmt($getNames);

        return $items;
    }

    public function queryArray($sql = '')
    {
        $getNames = sqlsrv_query($this->connection, $sql);
        if ($getNames === false) {
            die(ver(['sql' => $sql, 'errors' => sqlsrv_errors()], true));
        }
        if (!$getNames) {
            return [];
        }
        $items = [];

        while ($row = sqlsrv_fetch_array($getNames, SQLSRV_FETCH_ASSOC)) {
            $items[] = $row;
        }
        sqlsrv_free_stmt($getNames);

        return $items;
    }

    public function toPhinx()
    {
        return [];
    }
}
