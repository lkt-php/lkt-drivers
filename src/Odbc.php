<?php

namespace Lkt\Drivers;

class Odbc extends AbstractConnection
{
    public function __construct($connectionInfo = [])
    {
        parent::__construct($connectionInfo);

    }

    protected function connect()
    {
        $this->connection = \odbc_connect($this->host, $this->user, $this->password);

        if (!$this->connection) {
            $this->connection = \odbc_connect($this->host);
        }

        if ($this->connection && $this->database !== '') {
            \odbc_exec($this->connection, "USE {$this->database};");
        }
    }

    protected function disconnect()
    {
        \odbc_close($this->connection);
        $this->connection = null;
    }

    public function query($sql = '')
    {
        $this->connect();
        $sql = ConnectionHelper::sanitizeQuery($sql);

        $result = \odbc_exec($this->connection, $sql);
        $amount = \odbc_num_rows($result);
        $amount2 = \odbc_num_fields($result);
        $items = [];
        if ($amount > 0 || $amount2 > 0) {
            $columns = [];
            $columnAmount = \odbc_num_fields($result);
            for ($j = 1; $j <= $columnAmount; $j++) {
                $columns[] = \odbc_field_name($result, $j);
            }
            $l = \count($columns);
            if ($l > 0) {
                while (\odbc_fetch_row($result)) {
                    $item = [];
                    for ($i = 0; $i < \odbc_num_fields($result); ++$i) {
                        $item[$columns[$i]] = \odbc_result($result, $i + 1);
                    }
                    $items[] = (object)$item;
                }
            }
        }
        $this->disconnect();
        return $items;
    }

    public function queryArray($sql = '')
    {
        $this->connect();
        $sql = static::sanitizeQuery($sql);

        $result = \odbc_exec($this->connection, $sql);
        $amount = \odbc_num_rows($result);
        $amount2 = \odbc_num_fields($result);
        $items = [];
        if ($amount > 0 || $amount2 > 0) {
            $columns = [];
            $columnAmount = \odbc_num_fields($result);
            for ($j = 1; $j <= $columnAmount; $j++) {
                $columns[] = \odbc_field_name($result, $j);
            }
            $l = \count($columns);
            if ($l > 0) {
                while (\odbc_fetch_row($result)) {
                    $item = [];
                    for ($i = 0; $i < \odbc_num_fields($result); ++$i) {
                        $item[$columns[$i]] = \odbc_result($result, $i + 1);
                    }
                    $items[] = $item;
                }
            }
        }
        $this->disconnect();
        return $items;
    }

    public function toPhinx()
    {
        return [];
    }
}