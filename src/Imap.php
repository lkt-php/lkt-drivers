<?php

namespace Lkt\Drivers;


class Imap extends AbstractConnection
{
    const HOST_GMAIL = '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';

    public function __construct($connectionInfo = [])
    {
        $connectionInfo = $this->parseConnectionInfo($connectionInfo);
        parent::__construct($connectionInfo);
    }

    public function connect()
    {
        if ($this->connection !== null) {
            return;
        }
        // Perform the connection
        try {
            $this->connection = \imap_open($this->host, $this->user, $this->password, OP_READONLY) or die('Cannot connect to Gmail: ' . imap_last_error());

        } catch (\Exception $e) {
            die ('Connection to IMAP failed');
        }
    }

    public function disconnect()
    {
        $this->connection = null;
    }

    public function query($url = '', $withAttatchments = true)
    {
        $this->connect();

        /* Search Emails having the specified keyword in the email subject */
        $emailData = \imap_search($this->connection, $url);

        $items = [];
        foreach ($emailData as $emailIdent) {

            $overview = \imap_fetch_overview($this->connection, $emailIdent, 0);
            $message = \imap_fetchbody($this->connection, $emailIdent, '1.1');
            $messageExcerpt = substr($message, 0, 150);
            $partialMessage = trim(quoted_printable_decode($messageExcerpt));
            $date = date("d F, Y", strtotime($overview[0]->date));
            $attachments = array();

            if ($withAttatchments === true) {
                $structure = \imap_fetchstructure($this->connection, $emailIdent);
                if (isset($structure->parts) && count($structure->parts)) {
                    for ($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => '');

                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }

                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = \imap_fetchbody($this->connection, $emailIdent, $i + 1);
                            if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }
            }

            $items[] = [
                'overview' => $overview,
                'message' => $message,
                'messageExcerpt' => $messageExcerpt,
                'partialMessage' => $partialMessage,
                'date' => $date,
                'attachments' => $attachments,
            ];
        }

        return $items;
    }
}
